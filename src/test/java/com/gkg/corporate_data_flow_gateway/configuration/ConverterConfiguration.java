package com.gkg.corporate_data_flow_gateway.configuration;

import com.gkg.corporate_data_flow_gateway.mt4.converter.*;
import com.gkg.corporate_data_flow_gateway.mt4.converter.custom.ListToStringConverter;
import com.gkg.corporate_data_flow_gateway.mt5.converter.*;
import org.dozer.CustomConverter;
import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class ConverterConfiguration {

    @Bean
    public DozerBeanMapper mapper() {

        List<String> mappingFiles = Arrays.asList(
                "custom-dozer-mt4-model-mappings.xml"
        );

        List<CustomConverter> customerConverters = Arrays.asList(new ListToStringConverter(List.class, String.class));
        DozerBeanMapper mapper = new DozerBeanMapper();
        mapper.setMappingFiles(mappingFiles);
        mapper.setCustomConverters(customerConverters);
        return mapper;
    }

    @Bean
    public TradeRecordConverter tradeRecordConverter(DozerBeanMapper mapper){
        return new TradeRecordConverter(mapper());
    }
    @Bean
    public UserRecordConverter userRecordConverter(DozerBeanMapper mapper){
        return new UserRecordConverter(mapper());
    }
    @Bean
    public DailyReportConverter dailyReportConverter(DozerBeanMapper mapper){
        return new DailyReportConverter(mapper());
    }
    @Bean
    public ConGroupSecConverter conGroupSecConverter(DozerBeanMapper mapper){
        return new ConGroupSecConverter(mapper());
    }
    @Bean
    public ConGroupMarginConverter conGroupMarginConverter(DozerBeanMapper mapper){
        return new ConGroupMarginConverter(mapper());
    }
    @Bean
    public ConGroupConverter conGroupConverter(DozerBeanMapper mapper){
        return new ConGroupConverter(mapper());
    }
    @Bean
    public ConSessionTradeConverter conSessionTradeConverter(DozerBeanMapper mapper){
        return new ConSessionTradeConverter(mapper());
    }
    @Bean
    public ConSessionQuoteConverter conSessionQuoteConverter(DozerBeanMapper mapper){
        return new ConSessionQuoteConverter(mapper());
    }
    @Bean
    public ConSessionsConverter conSessionsConverter(DozerBeanMapper mapper){
        return new ConSessionsConverter(mapper());
    }
    @Bean
    public ConSymbolConverter conSymbolConverter(DozerBeanMapper mapper){
        return new ConSymbolConverter(mapper());
    }

    /* MT5 Converter config start */
    @Bean
    public ImtConCommissionConverter imtConCommissionConverter(DozerBeanMapper mapper){
        return new ImtConCommissionConverter(mapper());
    }

    @Bean
    public ImtConComTierConverter imtConComTierConverter(DozerBeanMapper mapper){
        return new ImtConComTierConverter(mapper());
    }
    @Bean
    public ImtConGroupConverter imtConGroupConverter(DozerBeanMapper mapper){
        return new ImtConGroupConverter(mapper());
    }
    @Bean
    public ImtConGroupSymbolConverter imtConGroupSymbolConverter(DozerBeanMapper mapper){
        return new ImtConGroupSymbolConverter(mapper());
    }
    @Bean
    public ImtConSymbolConverter imtConSymbolConverter(DozerBeanMapper mapper){
        return new ImtConSymbolConverter(mapper());
    }
    @Bean
    public ImtConSymbolSessionConverter imtConSymbolSessionConverter(DozerBeanMapper mapper){
        return new ImtConSymbolSessionConverter(mapper());
    }
    @Bean
    public ImtDealConverter imtDealConverter(DozerBeanMapper mapper){
        return new ImtDealConverter(mapper());
    }
    @Bean
    public ImtOrderConverter imtOrderConverter(DozerBeanMapper mapper){
        return new ImtOrderConverter(mapper());
    }
    @Bean
    public ImtPositionConverter imtPositionConverter(DozerBeanMapper mapper){
        return new ImtPositionConverter(mapper());
    }
    @Bean
    public ImtUserConverter imtUserConverter(DozerBeanMapper mapper){
        return new ImtUserConverter(mapper());
    }
}
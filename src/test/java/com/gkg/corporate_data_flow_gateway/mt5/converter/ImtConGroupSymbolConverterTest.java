package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConGroupSymbol;
import com.gkg.mosDtoLibrary.mt5.GroupSymbolDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigInteger;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtConGroupSymbolConverterTest {
    @Autowired
    private ImtConGroupSymbolConverter imtConGroupSymbolConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapGroupSymbolDtoToEntityCorrectly(){
        GroupSymbolDto source = new GroupSymbolDto();
        source.setExecMode(1l);
        source.setFreezeLevel(0);
        source.setIeVolumeMax(BigInteger.TEN);

        ImtConGroupSymbol dest = imtConGroupSymbolConverter.to(source);

        Assertions.assertEquals(dest.getExecMode(), 1l);
        Assertions.assertEquals(dest.getFreezeLevel(), 0);
        Assertions.assertEquals(dest.getIeVolumeMax(), BigInteger.TEN);
    }
}

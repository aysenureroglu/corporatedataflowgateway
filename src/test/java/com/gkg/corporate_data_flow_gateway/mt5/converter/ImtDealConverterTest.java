package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtDeal;
import com.gkg.mosDtoLibrary.mt5.DealDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigInteger;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtDealConverterTest {

    @Autowired
    private ImtDealConverter imtDealConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapDealDtoToEntityCorrectly(){
        DealDto source = new DealDto();
        source.setAction(2l);
        source.setComment("deal");
        source.setDealer(BigInteger.ONE);

        ImtDeal dest = imtDealConverter.to(source);

        Assertions.assertEquals(dest.getAction(), 2l);
        Assertions.assertEquals(dest.getComment(), "deal");
        Assertions.assertEquals(dest.getDealer(), BigInteger.ONE);
    }
}

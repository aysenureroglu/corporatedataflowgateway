package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtUser;
import com.gkg.mosDtoLibrary.mt5.UserDto;
import org.h2.engine.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtUserConverterTest {

    @Autowired
    private ImtUserConverter imtUserConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapUserDtoToEntityCorrectly(){
        UserDto source = new UserDto();
        source.setAccount("ABC");
        source.setLanguage(1l);

        ImtUser dest = imtUserConverter.to(source);

        Assertions.assertEquals(dest.getAccount(), "ABC");
        Assertions.assertEquals(dest.getLanguage(), 1l);
    }
}

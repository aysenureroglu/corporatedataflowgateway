package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.converter.ImtOrderConverter;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtOrder;
import com.gkg.mosDtoLibrary.mt5.OrderDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtOrderConverterTest {

    @Autowired
    private ImtOrderConverter imtOrderConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapOrderDtoToEntityCorrectly(){
        OrderDto source = new OrderDto();
        source.setComment("order");
        source.setDigits(1l);

        ImtOrder dest = imtOrderConverter.to(source);

        Assertions.assertEquals(dest.getComment(), "order");
        Assertions.assertEquals(dest.getDigits(), 1l);
    }
}

package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConSymbol;
import com.gkg.mosDtoLibrary.mt5.SymbolDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtConSymbolConverterTest {

    @Autowired
    private ImtConSymbolConverter imtConSymbolConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapSymbolDtoToEntityCorrectly(){
        SymbolDto source = new SymbolDto();
        source.setCategory("USDEUR");
        source.setPriceSettle(1.23);
        source.setIsin("asd");

        ImtConSymbol dest = imtConSymbolConverter.to(source);

        Assertions.assertEquals(dest.getCategory(), "USDEUR");
        Assertions.assertEquals(dest.getPriceSettle(), 1.23);
        Assertions.assertEquals(dest.getIsin(), "asd");
    }
}

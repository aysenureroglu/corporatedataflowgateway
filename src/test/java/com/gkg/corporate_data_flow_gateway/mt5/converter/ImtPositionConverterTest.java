package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtPosition;
import com.gkg.mosDtoLibrary.mt5.PositionDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigInteger;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtPositionConverterTest {
    @Autowired
    private ImtPositionConverter imtPositionConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapPositionDtoToEntityCorrectly(){
        PositionDto source = new PositionDto();
        source.setMetatraderId("123");
        source.setPosition(BigInteger.ONE);

        ImtPosition dest = imtPositionConverter.to(source);

        Assertions.assertEquals(dest.getMetatraderId(), "123");
        Assertions.assertEquals(dest.getPosition(), BigInteger.ONE);
    }
}

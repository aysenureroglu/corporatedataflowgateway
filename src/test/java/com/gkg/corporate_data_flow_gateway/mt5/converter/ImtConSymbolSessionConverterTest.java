package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConSymbolSession;
import com.gkg.mosDtoLibrary.mt5.SymbolSessionDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtConSymbolSessionConverterTest {

    @Autowired
    private ImtConSymbolSessionConverter imtConSymbolSessionConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapSymbolSessionDtoToEntityCorrectly(){
        SymbolSessionDto source = new SymbolSessionDto();
        source.setClose(0l);
        source.setMetatraderId("123");

        ImtConSymbolSession dest = imtConSymbolSessionConverter.to(source);

        Assertions.assertEquals(dest.getClose(), 0l);
        Assertions.assertEquals(dest.getMetatraderId(), "123");
    }
}

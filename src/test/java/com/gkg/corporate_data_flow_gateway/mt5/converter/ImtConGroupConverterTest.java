package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConGroup;
import com.gkg.mosDtoLibrary.mt5.GroupDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigInteger;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtConGroupConverterTest {

    @Autowired
    private ImtConGroupConverter imtConGroupConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapGroupDtoToEntityCorrectly(){
        GroupDto source = new GroupDto();
        source.setCompany("GKG");
        source.setCommissionTotal(5l);
        source.setMarginFlags(BigInteger.ONE);
        source.setTradeFlags(BigInteger.ZERO);

        ImtConGroup dest = imtConGroupConverter.to(source);

        Assertions.assertEquals(dest.getCompany(), "GKG");
        Assertions.assertEquals(dest.getCommissionTotal(), 5l);
        Assertions.assertEquals(dest.getMarginFlags(), BigInteger.ONE);
        Assertions.assertEquals(dest.getTradeFlags(), BigInteger.ZERO);
    }
}

package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConComTier;
import com.gkg.mosDtoLibrary.mt5.ComTierDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtConComTierConverterTest {

    @Autowired
    private ImtConComTierConverter imtConComTierConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapComTierDtoToEntityCorrectly(){
        ComTierDto source = new ComTierDto();
        source.setCurrency("USD");
        source.setMetatraderId("123");
        source.setRangeFrom(1.2);
        source.setRangeTo(2.1);

        ImtConComTier dest = imtConComTierConverter.to(source);

        Assertions.assertEquals(dest.getCurrency(), "USD");
        Assertions.assertEquals(dest.getMetatraderId(), "123");
        Assertions.assertEquals(dest.getRangeFrom(), 1.2);
        Assertions.assertEquals(dest.getRangeTo(), 2.1);
    }
}

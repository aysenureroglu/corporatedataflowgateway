package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConCommission;
import com.gkg.mosDtoLibrary.mt5.CommissionDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ImtConCommissionConverterTest {

    @Autowired
    private ImtConCommissionConverter imtConCommissionConverter;

    @BeforeAll
    public static void init(){

    }

    @Test
    public void shouldMapCommissionDtoToEntityCorrectly(){
        CommissionDto source = new CommissionDto();
        source.setChargeMode(1l);
        source.setDescription("test");
        source.setTurnoverCurrency("Euro");
        source.setMetatraderId("123");

        ImtConCommission dest = imtConCommissionConverter.to(source);

        Assertions.assertEquals(dest.getChargeMode(), 1l);
        Assertions.assertEquals(dest.getDescription(), "test");
        Assertions.assertEquals(dest.getTurnoverCurrency(), "Euro");
        Assertions.assertEquals(dest.getMetatraderId(), "123");
    }
}

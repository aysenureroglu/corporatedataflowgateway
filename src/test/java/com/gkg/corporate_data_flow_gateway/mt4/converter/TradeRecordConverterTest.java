package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.TradeRecord;
import com.gkg.mosDtoLibrary.mt4.TradeRecordDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class TradeRecordConverterTest {

    @Autowired
    private TradeRecordConverter tradeRecordConverter;

    @BeforeAll
    public static void init(){
    }

    @Test
    public void shouldMapTradeRecordDtoToEntityCorrectly() {
        TradeRecordDto source = new TradeRecordDto();
        source.setActivation(1);
        source.setClosePrice(100.50);
        source.setComment("test comment");
        source.setOrder(1);

        TradeRecord dest = tradeRecordConverter.to(source);

        Assertions.assertEquals(dest.getActivation(), 1);
        Assertions.assertEquals(dest.getClosePrice(), 100.50);
        Assertions.assertEquals(dest.getComment(), "test comment");
        Assertions.assertEquals(dest.getOrderValue(), 1);
    }

}

package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.UserRecord;
import com.gkg.mosDtoLibrary.mt4.UserRecordDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class UserRecordConverterTest {

    @Autowired
    private UserRecordConverter userRecordConverter;

    public static List<Double> reserved;
    public static List<Integer> enableReserved;

    @BeforeAll
    public static void init() {
        reserved = new ArrayList<>();
        reserved.add(1.1);
        reserved.add(2.1);
        reserved.add(3.2);

        enableReserved = new ArrayList<>();
        enableReserved.add(1);
        enableReserved.add(2);
        enableReserved.add(3);
    }

    @Test
    public void shouldUserRecordDtoToEntityCorrectly(){
        UserRecordDto source = new UserRecordDto();
        source.setAddress("office");
        source.setCredit(1.6);
        source.setEnableReserved(enableReserved);
        source.setReserved(reserved);
        source.setGroup("gkg-group");

        UserRecord dest = userRecordConverter.to(source);

        Assertions.assertEquals(dest.getAddress(), "office");
        Assertions.assertEquals(dest.getReserved(), "[1.1,2.1,3.2]");
        Assertions.assertEquals(dest.getCredit(), 1.6);
        Assertions.assertEquals(dest.getGroupName(), "gkg-group");
        Assertions.assertEquals(dest.getEnableReserved(), "[1,2,3]");
    }
}

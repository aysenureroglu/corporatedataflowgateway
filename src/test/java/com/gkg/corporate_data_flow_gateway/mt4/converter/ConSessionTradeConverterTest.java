package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.Trade;
import com.gkg.mosDtoLibrary.mt4.ConSessionDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ConSessionTradeConverterTest {

    @Autowired
    ConSessionTradeConverter conSessionTradeConverter;

    public static List<Integer> reserved;
    public static List<Integer> align;

    @BeforeAll
    public static void init() {
        reserved = new ArrayList<>();
        reserved.add(1);
        reserved.add(2);
        reserved.add(3);

        align = new ArrayList<>();
        align.add(1);
        align.add(2);
        align.add(3);
    }

    @Test
    public void shouldMapConSessionDtoToTradeEntityCorrectly() {
        ConSessionDto source = new ConSessionDto();
        source.setMetatraderId("123");
        source.setOpen(1);
        source.setReserved(reserved);
        source.setAlign(align);

        Trade dest = conSessionTradeConverter.to(source);

        Assertions.assertEquals(dest.getMetatraderId(), "123");
        Assertions.assertEquals(dest.getOpen(), 1);
        Assertions.assertEquals(dest.getReserved(), "[1,2,3]");
        Assertions.assertEquals(dest.getAlign(), "[1,2,3]");
    }

}

package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.ConSymbol;
import com.gkg.mosDtoLibrary.mt4.ConSessionDto;
import com.gkg.mosDtoLibrary.mt4.ConSessionsDto;
import com.gkg.mosDtoLibrary.mt4.ConSymbolDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ConSymbolConverterTest {

    @Autowired
    ConSymbolConverter conSymbolConverter;

    public static List<Integer> unused;
    public static List<Integer> externalUnused;

    @BeforeAll
    public static void init() {
        unused = new ArrayList<>();
        unused.add(1);
        unused.add(2);
        unused.add(3);

        externalUnused = new ArrayList<>();
        externalUnused.add(1);
        externalUnused.add(2);
        externalUnused.add(3);
    }

    @Test
    public void  shouldMapConSymbolDtoToEntity_whenMappingNestedFieldsCorrectly_thenCorrect(){
        ConSymbolDto conSymbolDto = new ConSymbolDto();
        conSymbolDto.setAskTickValue(2.1);
        conSymbolDto.setExpiration(123465788);
        conSymbolDto.setDescription("test");
        conSymbolDto.setUnused(unused);
        conSymbolDto.setExternalUnused(externalUnused);

        ConSessionsDto conSessions = new ConSessionsDto();
        conSessions.setMetatraderId("123");
        conSessions.setQuoteOvernight(1);
        conSessions.setReserved(unused);

        ConSessionDto nestedDto = new ConSessionDto();
        nestedDto.setClose(0);
        nestedDto.setMetatraderId("123");
        nestedDto.setReserved(unused);
        nestedDto.setAlign(unused);

        List<ConSessionDto> sessionDtoList = new ArrayList<>();
        sessionDtoList.add(nestedDto);
        conSessions.setSessions(sessionDtoList);

        List<ConSessionsDto> ssDtolist = new ArrayList<>();
        ssDtolist.add(conSessions);
        conSymbolDto.setSessions(ssDtolist);

        ConSymbol dest = conSymbolConverter.to(conSymbolDto);
        Assertions.assertEquals(dest.getAskTickValue(), 2.1);
        Assertions.assertEquals(dest.getSessions().get(0).getMetatraderId(), "123");
        //Assertions.assertEquals(dest.getSessions().get(0).getSessions().get(0).getClose(), 0);
        Assertions.assertEquals(dest.getUnused(), "[1,2,3]");
    }
}

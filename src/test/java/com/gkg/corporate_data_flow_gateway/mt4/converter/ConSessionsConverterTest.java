package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.ConSessions;
import com.gkg.mosDtoLibrary.mt4.ConSessionDto;
import com.gkg.mosDtoLibrary.mt4.ConSessionsDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ConSessionsConverterTest {

    @Autowired
    ConSessionsConverter conSessionsConverter;

    public static List<Integer> reserved;

    @BeforeAll
    public static void init() {
        reserved = new ArrayList<>();
        reserved.add(1);
        reserved.add(2);
        reserved.add(3);
    }

    @Test
    public void shouldMapConSessionsDtoToEntity_whenMappingNestedFieldsCorrectly_thenCorrect() {
        ConSessionsDto source = new ConSessionsDto();
        source.setMetatraderId("123");
        source.setQuoteOvernight(1);
        source.setReserved(reserved);

        ConSessionDto nestedDto = new ConSessionDto();
        nestedDto.setClose(0);
        nestedDto.setMetatraderId("123");
        nestedDto.setReserved(reserved);
        nestedDto.setAlign(reserved);

        List<ConSessionDto> nestedDtoList = new ArrayList<>();
        nestedDtoList.add(nestedDto);
        //source.setSessions(nestedDtoList);

        ConSessions dest = conSessionsConverter.to(source);

        Assertions.assertEquals(dest.getReserved(), "[1,2,3]");
        //Assertions.assertEquals(dest.getSessions().get(0).getMetatraderId(), "123");
    }
}
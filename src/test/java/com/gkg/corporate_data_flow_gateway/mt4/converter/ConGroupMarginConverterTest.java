package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.ConGroupMargin;
import com.gkg.mosDtoLibrary.mt4.ConGroupMarginDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ConGroupMarginConverterTest {

    @Autowired
    ConGroupMarginConverter conGroupMarginConverter;

    public static List<Integer> reserved;

    @BeforeAll
    public static void init() {
        reserved = new ArrayList<>();
        reserved.add(1);
        reserved.add(2);
        reserved.add(3);
    }

    @Test
    public void shouldMapConGroupMarginDtoToEntityCorrectly(){
        ConGroupMarginDto source = new ConGroupMarginDto();
        source.setMetatraderId("123");
        source.setReserved(reserved);
        source.setSwapLong(1.4);

        ConGroupMargin dest = conGroupMarginConverter.to(source);

        Assertions.assertEquals(dest.getReserved(), "[1,2,3]");
        Assertions.assertEquals(dest.getMetatraderId(), "123");
        Assertions.assertEquals(dest.getSwapLong(), 1.4);


        source = conGroupMarginConverter.from(dest);
        Assertions.assertEquals(source.getMetatraderId(), "123");
        Assertions.assertEquals(source.getSwapLong(), 1.4);
        Assertions.assertEquals(source.getReserved().get(0), 1);
    }
}

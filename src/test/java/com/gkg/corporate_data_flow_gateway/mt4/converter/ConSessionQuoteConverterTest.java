package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.Quote;
import com.gkg.corporate_data_flow_gateway.mt4.entity.Trade;
import com.gkg.mosDtoLibrary.mt4.ConSessionDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ConSessionQuoteConverterTest {

    @Autowired
    ConSessionQuoteConverter conSessionQuoteConverter;

    public static List<Integer> reserved;
    public static List<Integer> align;

    @BeforeAll
    public static void init() {
        reserved = new ArrayList<>();
        reserved.add(1);
        reserved.add(2);
        reserved.add(3);

        align = new ArrayList<>();
        align.add(1);
        align.add(2);
        align.add(3);
    }

    @Test
    public void shouldMapConSessionDtoToQuoteEntityCorrectly() {
        ConSessionDto source = new ConSessionDto();
        source.setMetatraderId("123");
        source.setOpen(1);
        source.setReserved(reserved);
        source.setAlign(align);

        Quote dest = conSessionQuoteConverter.to(source);

        Assertions.assertEquals(dest.getMetatraderId(), "123");
    }

}

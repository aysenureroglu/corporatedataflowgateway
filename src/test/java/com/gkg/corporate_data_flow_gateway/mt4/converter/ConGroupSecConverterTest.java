package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.ConGroupSec;
import com.gkg.mosDtoLibrary.mt4.ConGroupSecDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ConGroupSecConverterTest {

    @Autowired
    ConGroupSecConverter conGroupSecConverter;

    public static List<Integer> reserved;

    @BeforeAll
    public static void init() {
        reserved = new ArrayList<>();
        reserved.add(1);
        reserved.add(2);
        reserved.add(3);
    }

    @Test
    public void shouldMapConGroupSecDtoToEntityCorrectly(){
        ConGroupSecDto source = new ConGroupSecDto();
        source.setAutoCloseoutMode(1);
        source.setCommTax(1.2);
        source.setReserved(reserved);
        source.setShow(1);
        source.setExecution(2);

        ConGroupSec dest = conGroupSecConverter.to(source);

        Assertions.assertEquals(dest.getAutoCloseoutMode(), 1);
        Assertions.assertEquals(dest.getCommTax(), 1.2);
        Assertions.assertEquals(dest.getReserved(), "[1,2,3]");
        Assertions.assertEquals(dest.getShowValue(), 1);
        Assertions.assertEquals(dest.getExecutionNum(), 2);

    }

}

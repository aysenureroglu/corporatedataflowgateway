package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.ConGroup;
import com.gkg.mosDtoLibrary.mt4.ConGroupDto;
import com.gkg.mosDtoLibrary.mt4.ConGroupMarginDto;
import com.gkg.mosDtoLibrary.mt4.ConGroupSecDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class ConGroupConverterTest {
    @Autowired
    ConGroupConverter conGroupConverter;

    public static List<Integer> reserved;
    public static List<Integer> unusedRights;
    public static List<Integer> newsLanguages;
    @BeforeAll
    public static void init(){
        reserved = new ArrayList<>();
        reserved.add(1);
        reserved.add(2);
        reserved.add(3);
        unusedRights = new ArrayList<>();
        unusedRights.add(1);
        unusedRights.add(2);
        unusedRights.add(3);
        newsLanguages = new ArrayList<>();
        newsLanguages.add(1);
        newsLanguages.add(2);
        newsLanguages.add(3);
    }

    @Test
    public void givenSourceObjectAndDestClass_whenMapsDifferentTypeFieldsCorrectly_thenCorrect(){
        ConGroupDto source = new ConGroupDto();

        source.setAdvSecurity(4);
        source.setCurrency("USD");
        source.setUnusedRights(unusedRights);
        source.setNewsLanguages(newsLanguages);
        source.setReserved(reserved);

        ConGroup dest = conGroupConverter.to(source);
        System.out.println(dest.getReserved());
        System.out.println(dest.getAdvSecurity());
        System.out.println(dest.getCurrency());
        Assertions.assertEquals(dest.getReserved(), "[1,2,3]");
        Assertions.assertEquals(dest.getUnusedRights(), "[1,2,3]");
        Assertions.assertEquals(dest.getNewsLanguages(), "[1,2,3]");
    }

    @Test
    public void shouldMapConGroupDtoToEntity_whenMappingNestedFieldsCorrectly_thenCorrect(){
        ConGroupDto source = new ConGroupDto();
        source.setUnusedRights(unusedRights);
        source.setNewsLanguages(newsLanguages);
        source.setReserved(reserved);
        source.setGroup("gkg-group");

        ConGroupSecDto secDto = new ConGroupSecDto();
        secDto.setCommTax(1.2);
        secDto.setAutoCloseoutMode(1);
        secDto.setReserved(reserved);
        secDto.setShow(1);
        secDto.setExecution(3);
        List<ConGroupSecDto> secDtoList = new ArrayList<>();
        secDtoList.add(secDto);

        source.setSecGroups(secDtoList);

        ConGroupMarginDto marginDto = new ConGroupMarginDto();
        marginDto.setReserved(reserved);
        marginDto.setSwapLong(1.4);
        List<ConGroupMarginDto> marginDtoList = new ArrayList<>();
        marginDtoList.add(marginDto);

        source.setSecMargins(marginDtoList);

        source.setAdvSecurity(4);
        source.setCurrency("USD");

        ConGroup dest = conGroupConverter.to(source);
        Assertions.assertEquals(dest.getReserved(), "[1,2,3]");
        Assertions.assertEquals(dest.getGroupName(), "gkg-group");
        Assertions.assertEquals(dest.getSecGroups().get(0).getCommTax(), 1.2);
        Assertions.assertEquals(dest.getSecGroups().get(0).getReserved(), "[1,2,3]");
        Assertions.assertEquals(dest.getSecGroups().get(0).getShowValue(), 1);
        Assertions.assertEquals(dest.getSecGroups().get(0).getExecutionNum(), 3);
        Assertions.assertEquals(dest.getSecMargins().get(0).getReserved(), "[1,2,3]");
        //Assertions.assertEquals(dest.getClosePrice(), 100.50);
        //Assertions.assertTrue(true);
    }
}

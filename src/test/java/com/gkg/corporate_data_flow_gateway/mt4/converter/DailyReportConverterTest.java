package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.configuration.ConverterConfiguration;
import com.gkg.corporate_data_flow_gateway.mt4.entity.DailyReport;
import com.gkg.mosDtoLibrary.mt4.DailyReportDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {ConverterConfiguration.class})
public class DailyReportConverterTest {

    @Autowired
    public DailyReportConverter dailyReportConverter;

    public static List<Integer> reserved;
    @BeforeAll
    public static void init(){
        reserved = new ArrayList<>() ;
        reserved.add(1);
        reserved.add(2);
        reserved.add(3);
    }

    @Test
    public void shouldMapDailyReportDtoToEntityCorrectly(){
        DailyReportDto source = new DailyReportDto();

        source.setBalance(100.5);
        source.setBank("GKG");
        source.setCtm(100200);
        source.setLogin(1);
        source.setReserved(reserved);
        source.setGroup("gkg-group");

        DailyReport dest = dailyReportConverter.to(source);

        Assertions.assertEquals(dest.getBalance(), 100.5);
        Assertions.assertEquals(dest.getBank(), "GKG");
        Assertions.assertEquals(dest.getCtm(), 100200);
        Assertions.assertEquals(dest.getLogin(), 1);
        Assertions.assertEquals(dest.getReserved(), "[1,2,3]");
        Assertions.assertEquals(dest.getGroupName(), "gkg-group");

    }
}

package com.gkg.corporate_data_flow_gateway.mt4.repository;


import com.gkg.corporate_data_flow_gateway.mt4.entity.ConSessions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ConSessionsRepositoryTest {
    @Autowired
    private ConSessionsRepository repository;

    @Test
    public void shouldCreateConSessionsWithAutoGeneratedId(){
        ConSessions conSessions = repository.save(new ConSessions());
        assertThat(conSessions.getId()).isGreaterThan(0);
    }

    @Test
    public void shouldFindUConSessionsById(){
        ConSessions conSessions = new ConSessions();
        conSessions.setMetatraderId("metaId1");

        repository.save(conSessions);
        Optional<ConSessions> foundConSessions = repository.findById(conSessions.getId());

        assertThat(foundConSessions.get().getId()).isEqualTo(conSessions.getId());
        assertThat(foundConSessions.get().getMetatraderId()).isEqualTo(conSessions.getMetatraderId());
    }

    @Test
    public void shouldDeleteConSessions(){
        ConSessions conSessions = new ConSessions();

        repository.save(conSessions);
        repository.delete(conSessions);
        ConSessions foundConSessions = repository.findById(conSessions.getId()).orElse(null);

        assertThat(foundConSessions).isEqualTo(null);
    }
}

package com.gkg.corporate_data_flow_gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorporateDataFlowGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CorporateDataFlowGatewayApplication.class, args);
    }

}

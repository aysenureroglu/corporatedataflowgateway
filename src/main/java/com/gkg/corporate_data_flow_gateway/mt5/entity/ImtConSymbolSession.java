package com.gkg.corporate_data_flow_gateway.mt5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtConSymbolSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;

    private long      open;
    private long      openHours;
    private long      openMinutes;
    private long      close;
    private long      closeHours;
    private long      closeMinutes;
}

package com.gkg.corporate_data_flow_gateway.mt5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtDeal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String comment;
    private String externalId; //deal ID in external trading systems
    private String gateway;
    private String print;
    private String symbol;

    private BigInteger deal; //deal ticket
    private BigInteger dealer; //login of a dealer, who has processed a deal
    private BigInteger expertId;
    private BigInteger flags;
    private BigInteger login; //login of the client, to whom the deal belongs
    private BigInteger orderName;
    private BigInteger positionId;
    private BigInteger volume;
    private BigInteger volumeClosed;
    private BigInteger volumeClosedExt;
    private BigInteger volumeExt;

    private long action;
    private long digits;
    private long digitsCurrency;
    private long entry;
    private long modificationFlags;
    private long reason;
    private long time;
    private long timeMsc;

    private double commission;
    private double contractSize;
    private double fee;
    private double obsoloteValue;
    private double price;
    private double priceGateway;
    private double pricePosition;
    private double priceSl;
    private double priceTp;
    private double profit;
    private double profitRaw;
    private double rateMargin;
    private double rateProfit;
    private double storage;
    private double tickSize;
    private double tickValue;
}

package com.gkg.corporate_data_flow_gateway.mt5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtPosition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String comment;
    private String externalId;
    private String print;
    private String symbol;

    private BigInteger login; //Get the login of the client, to whom the trade position belongs
    private BigInteger position;
    private BigInteger dealer; //login of a dealer, who has processed the order that opened the position
    private BigInteger expertId;
    private BigInteger expertPositionId;
    private BigInteger volume;
    private BigInteger volumeExt;

    private long action; //position type
    private long activationFlags;
    private long activationMode;
    private long activationTime;
    private long digits;
    private long digitsCurrency;
    private long modificationFlags;
    private long reason;
    private long timeCreate;
    private long timeCreateMsc;
    private long timeUpdate;
    private long timeUpdateMsc;

    private double contractSize;
    private double activationPrice;
    private double obsoloteValue;
    private double priceCurrent;
    private double priceOpen;
    private double priceSl;
    private double priceTp;
    private double profit;
    private double rateMargin;
    private double rateProfit;
    private double storage;
}

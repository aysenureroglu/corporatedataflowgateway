package com.gkg.corporate_data_flow_gateway.mt5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String groupName;
    private String lastIp;
    private String name;
    private String company;
    private String account; //number of a client's account in an external trading system
    private String country;
    private String city;
    private String state;
    private String zipCode;
    private String address;
    private String phone;
    private String email;
    private String clientId; //number of a client's identity document.such as passport num
    private String status;
    private String comment;
    private String phonePassword;
    private String leadCampaign;
    private String leadCource;
    private String mqid;

    private BigInteger certSerialNumber;
    private BigInteger rights;
    private BigInteger agent;

    private long registration;
    private long lastAccess;
    private long language;
    private long color;
    private long leverage;
    private long externalAccountTotal;
    private long lastPassChange;

    private double balance;
    private double credit;
    private double interestRate;
    private double commissionDaily;
    private double commissionMonthly;
    private double commissionAgentDaily;
    private double commissionAgentMonthly;
    private double balancePrevDay;
    private double balancePrevMonth;
    private double equityPrevDay;
    private double equityPrevMonth;
}

package com.gkg.corporate_data_flow_gateway.mt5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtConGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String groupName;
    private String company;
    private String companyPage;
    private String companyEmail;
    private String companySupportPage;
    private String companySupportEmail;
    private String companyCatalog;
    private String currency;
    private String reportsSmtp;
    private String reportsSmtpLogin;
    private String reportsSmtpPass;
    private String newsCategory;

    private BigInteger server;
    private BigInteger permissionsFlags;
    private BigInteger reportsFlags;
    private BigInteger tradeFlags;
    private BigInteger marginFlags;

    private long authMode;
    private long authPasswordMin;
    private long currencyDigits;
    private long reportsMode;
    private long newsMode;
    private long newsLangTotal;
    private long newsLangNext;
    private long mailMode;
    private long marginFreeMode;
    private long marginSoMode;
    private long demoLeverage;
    private long limitHistory;
    private long limitOrders;
    private long limitSymbols;
    private long commissionTotal;
    private long symbolTotal;
    private long marginFreeProfitMode;
    private long marginMode;
    private long authOtpMode;
    private long tradeTransferMode;
    private long limitPositions;

    private double tradeInterestRate;
    private double tradeVirtualCredit;
    private double marginCall;
    private double marginStopOut;
    private double demoDeposit;
}

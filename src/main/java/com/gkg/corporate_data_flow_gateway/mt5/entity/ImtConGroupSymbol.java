package com.gkg.corporate_data_flow_gateway.mt5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtConGroupSymbol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String path;

    private int spreadDiff;
    private int spreadDiffDefault;
    private int spreadDiffBalance;
    private int spreadDiffBalanceDefault;
    private int stopsLevel;
    private int stopsLevelDefault;
    private int freezeLevel;
    private int freezeLevelDefault;
    private int swap3Day;
    private int swap3DayDefault;

    private BigInteger volumeMin;
    private BigInteger volumeMinDefault;
    private BigInteger volumeMax;
    private BigInteger volumeMaxDefault;
    private BigInteger volumeStep;
    private BigInteger volumeStepDefault;
    private BigInteger volumeLimit;
    private BigInteger volumeLimitDefault;
    private BigInteger ieVolumeMax;
    private BigInteger ieVolumeMaxDefault;
    private BigInteger volumeMinExt;
    private BigInteger volumeMinExtDefault;
    private BigInteger volumeMaxExt;
    private BigInteger volumeMaxExtDefault;
    private BigInteger volumeStepExt;
    private BigInteger volumeStepExtDefault;
    private BigInteger volumeLimitExt;
    private BigInteger volumeLimitExtDefault;
    private BigInteger ieVolumeMaxExt;
    private BigInteger ieVolumeMaxExtDefault;

    private long tradeMode;
    private long tradeModeDefault;
    private long execMode;
    private long execModeDefault;
    private long fillFlags;
    private long fillFlagsDefault;
    private long expiryFlags;
    private long expiryFlagsDefault;
    private long marginFlags;
    private long marginFlagsDefault;
    private long swapMode;
    private long swapModeDefault;
    private long reTimeout;
    private long reTimeoutDefault;
    private long ieCheckMode;
    private long ieCheckModeDefault;
    private long ieTimeout;
    private long ieTimeoutDefault;
    private long ieSlipProfit;
    private long ieSlipProfitDefault;
    private long ieSlipLosing;
    private long ieSlipLosingDefault;
    private long orderFlags;
    private long orderFlagsDefault;
    private long reFlags;
    private long reFlagsDefault;
    private long permissionsFlags;
    private long bookDepthLimit;
    private long ieFlags;
    private long ieFlagsDefault;

    private double marginInitial;
    private double marginInitialDefault;
    private double marginMaintenance;
    private double marginMaintenanceDefault;
    private double marginLong;
    private double marginLongDefault;
    private double marginShort;
    private double marginShortDefault;
    private double marginLimit;
    private double marginLimitDefault;
    private double marginStop;
    private double marginStopDefault;
    private double marginStopLimit;
    private double marginStopLimitDefault;
    private double swapLong;
    private double swapLongDefault;
    private double swapShort;
    private double swapShortDefault;
    private double marginRateInitial;
    private double marginRateInitialDefault;
    private double marginRateMaintenance;
    private double marginRateMaintenanceDefault;
    private double marginRateLiquidity;
    private double marginRateLiquidityDefault;
    private double marginHedged;
    private double marginHedgedDefault;
    private double marginRateCurrency;
    private double marginRateCurrencyDefault;
}

package com.gkg.corporate_data_flow_gateway.mt5.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtConSymbol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String basis;
    private String category;
    private String cfi;
    private String currencyBase;
    private String currencyMargin;
    private String currencyProfit;
    private String description;
    private String exchange;
    private String international;
    private String isin;
    private String page;
    private String path;
    private String source;
    private String symbol;

    private int calcMode;
    private int chartMode;
    private int execMode;
    private int expiryFlags;
    private int fillFlags;
    private int freezeLevel;
    private int gcMode;
    private int marginFlags;
    private int optionsMode;
    private int orderFlags;
    private int spliceTimeType;
    private int spliceType;
    private int spreadBalance;
    private int spreadDiff;
    private int spreadDiffBalance;
    private int stopsLevel;
    private int tickFlags;
    private int tradeFlags;

    private BigInteger ieVolumeMax;
    private BigInteger ieVolumeMaxExt;
    private BigInteger volumeLimit;
    private BigInteger volumeLimitExt;
    private BigInteger volumeMax;
    private BigInteger volumeMaxExt;
    private BigInteger volumeMin;
    private BigInteger volumeMinExt;
    private BigInteger volumeStep;
    private BigInteger volumeStepExt;

    private long color;
    private long colorBackground;
    private long currencyBaseDigits;
    private long currencyMarginDigits;
    private long currencyProfitDigits;
    private long digits;
    private long filterDiscard;
    private long filterGap;
    private long filterGapTicks;
    private long filterHard;
    private long filterHardTicks;
    private long filterSoft;
    private long filterSoftTicks;
    private long filterSpreadMax;
    private long filterSpreadMin;
    private long ieCheckMode;
    private long ieFlags;
    private long ieSlipLosing;
    private long ieSlipProfit;
    private long ieTimeout;
    private long quotesTimeout;
    private long reFlags;
    private long reTimeout;
    private long spliceTimeDays;
    private long spread;
    private long swap3Day;
    private long swapMode;
    private long tickBookDepth;
    private long timeExpiration;
    private long timeStart;

    private double accruedInterest;
    private double contractSize;
    private double faceValue;
    private double marginHedged;
    private double marginInitial;
    private double marginLimit;
    private double marginLong;
    private double marginMaintenance;
    private double marginRateCurrency;
    private double marginRateLiquidity;
    private double marginShort;
    private double marginStop;
    private double marginStopLimit;
    private double multiply;
    private double point;
    private double priceLimitMax;
    private double priceLimitMin;
    private double priceSettle;
    private double priceStrike;
    private double swapLong;
    private double swapShort;
    private double tickSize;
    private double tickValue;
}

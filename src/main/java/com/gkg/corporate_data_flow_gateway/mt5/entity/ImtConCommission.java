package com.gkg.corporate_data_flow_gateway.mt5.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtConCommission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String name;
    private String description;
    private String path;
    private String turnoverCurrency;

    private long mode;
    private long rangeMode;
    private long chargeMode;
    private long tierTotal;
    private long entryMode;
}

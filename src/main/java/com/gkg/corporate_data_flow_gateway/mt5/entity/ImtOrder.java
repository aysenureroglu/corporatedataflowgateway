package com.gkg.corporate_data_flow_gateway.mt5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ImtOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String Comment;
    private String externalId;//order ID in external trading systems
    private String print;
    private String symbol;

    private BigInteger dealer; //login of a dealer, who has processed an order
    private BigInteger expertId;
    private BigInteger login; //login of the client, to whom the order belongs
    private BigInteger orderName; //order ticket
    private BigInteger positionById;
    private BigInteger positionId;
    private BigInteger volumeCurrent;
    private BigInteger volumeCurrentExt;
    private BigInteger volumeInitial;
    private BigInteger volumeInitialExt;

    private long activationFlags;
    private long activationMode;
    private long activationTime;
    private long digits;
    private long digitsCurrency;
    private long modificationFlags;
    private long reason;
    private long state;
    private long timeDone;
    private long timeDoneMsc;
    private long timeExpiration;
    private long timeSetup;
    private long timeSetupMsc;
    private long type; //order type
    private long typeFill; //order filling type
    private long typeTime;

    private double activationPrice;
    private double contractSize;
    private double priceCurrent;
    private double priceOrder;
    private double priceSl;
    private double priceTp;
    private double priceTrigger;
    private double rateMargin;
}

package com.gkg.corporate_data_flow_gateway.mt5.repository;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<ImtPosition, Integer> {
}

package com.gkg.corporate_data_flow_gateway.mt5.repository;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConSymbolSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SymbolSessionRepository extends JpaRepository<ImtConSymbolSession, Integer> {
}

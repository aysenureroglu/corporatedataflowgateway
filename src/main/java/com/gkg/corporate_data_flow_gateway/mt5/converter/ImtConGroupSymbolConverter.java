package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConGroup;
import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConGroupSymbol;
import com.gkg.mosDtoLibrary.mt5.GroupDto;
import com.gkg.mosDtoLibrary.mt5.GroupSymbolDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ImtConGroupSymbolConverter {

    private DozerBeanMapper mapper;

    public ImtConGroupSymbol to(GroupSymbolDto dto){
        return mapper.map(dto, ImtConGroupSymbol.class);
    }

    public GroupSymbolDto from(ImtConGroupSymbol entity){
        return mapper.map(entity, GroupSymbolDto.class);
    }
}

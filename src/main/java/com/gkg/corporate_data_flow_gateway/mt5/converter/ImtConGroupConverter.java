package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConGroup;
import com.gkg.mosDtoLibrary.mt5.GroupDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ImtConGroupConverter {

    private DozerBeanMapper mapper;

    public GroupDto from(ImtConGroup entity){
        return mapper.map(entity, GroupDto.class);
    }

    public ImtConGroup to(GroupDto dto){
        return mapper.map(dto, ImtConGroup.class);
    }
}

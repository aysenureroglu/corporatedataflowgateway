package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConSymbolSession;
import com.gkg.mosDtoLibrary.mt5.SymbolSessionDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;

@AllArgsConstructor
public class ImtConSymbolSessionConverter {

    private DozerBeanMapper mapper;

    public ImtConSymbolSession to(SymbolSessionDto dto){
        return mapper.map(dto, ImtConSymbolSession.class);
    }

    public SymbolSessionDto from(ImtConSymbolSession entity){
        return mapper.map(entity, SymbolSessionDto.class);
    }
}

package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtUser;
import com.gkg.mosDtoLibrary.mt5.UserDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;

@AllArgsConstructor
public class ImtUserConverter {

    private DozerBeanMapper mapper;

    public ImtUser to(UserDto dto){
        return mapper.map(dto, ImtUser.class);
    }

    public UserDto from(ImtUser entity){
        return mapper.map(entity, UserDto.class);
    }
}

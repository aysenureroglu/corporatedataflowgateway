package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtPosition;
import com.gkg.mosDtoLibrary.mt5.PositionDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ImtPositionConverter {

    private DozerBeanMapper mapper;

    public ImtPosition to(PositionDto dto){
        return mapper.map(dto, ImtPosition.class);
    }

    public PositionDto from(ImtPosition entity){
        return mapper.map(entity, PositionDto.class);
    }
}

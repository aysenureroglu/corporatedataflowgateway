package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConSymbol;
import com.gkg.mosDtoLibrary.mt5.SymbolDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ImtConSymbolConverter {

    private DozerBeanMapper mapper;

    public ImtConSymbol to(SymbolDto dto){
        return mapper.map(dto, ImtConSymbol.class);
    }

    public SymbolDto from(ImtConSymbol entity) {
        return mapper.map(entity, SymbolDto.class);
    }
}

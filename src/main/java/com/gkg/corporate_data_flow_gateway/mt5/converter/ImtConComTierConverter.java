package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConComTier;
import com.gkg.mosDtoLibrary.mt5.ComTierDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ImtConComTierConverter {

    private DozerBeanMapper mapper;

    public ComTierDto from(ImtConComTier entity){
        return mapper.map(entity, ComTierDto.class);
    }

    public ImtConComTier to(ComTierDto dto){
        return mapper.map(dto, ImtConComTier.class);
    }
}

package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtDeal;
import com.gkg.mosDtoLibrary.mt5.DealDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ImtDealConverter {

    private DozerBeanMapper mapper;

    public ImtDeal to(DealDto dto){
        return mapper.map(dto, ImtDeal.class);
    }

    public DealDto from(ImtDeal entity){
        return mapper.map(entity, DealDto.class);
    }
}

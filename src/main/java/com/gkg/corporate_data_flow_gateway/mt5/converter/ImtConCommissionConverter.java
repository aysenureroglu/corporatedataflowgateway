package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtConCommission;
import com.gkg.mosDtoLibrary.mt5.CommissionDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
@AllArgsConstructor
public class ImtConCommissionConverter {

    private DozerBeanMapper mapper;

    public ImtConCommission to(CommissionDto dto){
        return mapper.map(dto, ImtConCommission.class);
    }

    public CommissionDto from(ImtConCommission entity){
        return mapper.map(entity, CommissionDto.class);
    }
}

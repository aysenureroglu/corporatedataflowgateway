package com.gkg.corporate_data_flow_gateway.mt5.converter;

import com.gkg.corporate_data_flow_gateway.mt5.entity.ImtOrder;
import com.gkg.mosDtoLibrary.mt5.OrderDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;

@AllArgsConstructor
public class ImtOrderConverter {

    private DozerBeanMapper mapper;

    public ImtOrder to(OrderDto dto){
        return mapper.map(dto, ImtOrder.class);
    }

    public OrderDto from(ImtOrder entity){
        return mapper.map(entity, OrderDto.class);
    }
}

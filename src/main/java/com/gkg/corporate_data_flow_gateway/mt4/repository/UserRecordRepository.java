package com.gkg.corporate_data_flow_gateway.mt4.repository;

import com.gkg.corporate_data_flow_gateway.mt4.entity.UserRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRecordRepository extends JpaRepository<UserRecord, Integer> {
}

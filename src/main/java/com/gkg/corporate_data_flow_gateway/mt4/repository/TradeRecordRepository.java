package com.gkg.corporate_data_flow_gateway.mt4.repository;

import com.gkg.corporate_data_flow_gateway.mt4.entity.TradeRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeRecordRepository extends JpaRepository<TradeRecord, Integer> {
}

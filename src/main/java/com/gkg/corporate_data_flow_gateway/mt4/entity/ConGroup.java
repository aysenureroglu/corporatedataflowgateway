package com.gkg.corporate_data_flow_gateway.mt4.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
public class ConGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    @Mapping("group")
    private String groupName;
    private String company;
    private String signature;
    private String supportPage;
    private String smtpServer;
    private String smtpLogin; //The login for connection to the mail serve
    private String smtpPassword; //Password for connection to the mail server.
    private String supportEmail; //The email address of the company's technical support team
    private String templates;
    private String currency;
    private String securitiesHash;

    private int enable;
    private int timeout;
    private int advSecurity;
    private int copies;
    private int reports;
    private int defaultLeverage;
    private int maxSecurities;
    private int secMarginsTotal;
    private int marginCall;
    private int marginMode;
    private int marginStopOut;
    private int useSwap;
    private int news;
    private int rights;
    private int checkIePrices;
    private int maxPositions;
    private int close_Reopen;
    private int hedgeProhibited;
    private int closeFifo;
    private int hedgeLargeLeg;

    private int marginType;
    private int archivePeriod;
    private int archiveMaxBalance;
    private int stopoutSkipHedged;
    private int archivePendingPeriod;

    private long newsLanguagesTotal;

    private double defaultDeposit;
    private double credit;
    private double interestRate;

    @OneToMany(mappedBy = "conGroup")
    private List<ConGroupSec> secGroups;

    @OneToMany(mappedBy = "conGroup")
    private List<ConGroupMargin> secMargins;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String unusedRights; // it is sent as an int[2] by MT, so stored as json

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String newsLanguages; // it is sent as an int[8] by MT, so stored as json

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String reserved; // it is sent as an int[17] by MT, so stored as json
}

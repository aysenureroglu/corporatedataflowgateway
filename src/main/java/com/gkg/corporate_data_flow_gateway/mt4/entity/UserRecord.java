package com.gkg.corporate_data_flow_gateway.mt4.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    @Mapping("group")
    private String groupName;
    private String password;
    private String passwordInvestor;
    private String passwordPhone;
    private String name;
    private String country;
    private String city;
    private String state;
    private String zipcode;
    private String address;
    private String phone;
    private String email;
    private String comment;
    private String ssnId; //SSN (IRD)
    private String status;
    private String privateKey;
    private String unused;
    private String apiData;

    private int login;
    private int enable;
    private int enableChangePassword;
    private int enableReadOnly;
    private int leverage;
    private int agentAccount;
    private int sendReports;
    private int balanceStatus;
    private int userColor;

    private long timestamp;
    private long regDate;
    private long lastDate;

    private double balance;
    private double prevMonthBalance;
    private double prevBalance;
    private double credit;
    private double interestRate;
    private double taxes;
    private double prevMonthEquity;
    private double prevEquity;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String enableReserved; // it is sent as an int[2] by MT, so stored as json

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String reserved; // it is sent as an double[2] by MT, so stored as json
}

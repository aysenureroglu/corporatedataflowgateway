package com.gkg.corporate_data_flow_gateway.mt4.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class  ConSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //private Date transactionDate;

    private String metatraderId;
    private int open;
    private int close;
    private short openHour;
    private short openMin;
    private short closeHour;
    private short closeMin;

    @ManyToOne
    private ConSessions conSessions;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String reserved; // it is sent as an short[7] by MT, so stored as json

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String align; // it is sent as an short[7] by MT, so stored as json
}

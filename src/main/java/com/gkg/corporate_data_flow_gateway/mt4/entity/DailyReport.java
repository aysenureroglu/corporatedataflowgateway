package com.gkg.corporate_data_flow_gateway.mt4.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
public class DailyReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    @Mapping("group")
    private String groupName;
    private String bank;

    private int login;
    private int next;

    private long ctm;

    private double balancePrev;
    private double balance;
    private double deposit;
    private double credit;
    private double profitClosed;
    private double profit;
    private double equity;
    private double margin;
    private double marginFree;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String reserved; // it is sent as an int[3] by MT, so stored as json
}

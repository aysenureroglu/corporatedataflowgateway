package com.gkg.corporate_data_flow_gateway.mt4.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
public class ConSymbol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatrader_id;
    private String symbol;
    private String description;
    private String source;
    private String currency;
    private String margin_currency;

    private int type;
    private int digits;
    private int trade; //symbol trading mode
    private int backgroundColor;
    private int count;
    private int countOriginal;
    private int realtime;
    private int profitMode;
    private int profitReserved;
    private int filter;
    private int filterCounter;
    private int filterSmoothing;
    private int logging;
    private int spread;
    private int spreadBalance;
    private int exeMode;
    private int swapEnable;
    private int swapType;
    @Mapping("swapRollover3days")
    private int swapRollover;
    private int stopsLevel;
    private int gtcPendings;
    private int marginMode;
    private int longOnly;
    private int instantMaxVolume;
    private int freezeLevel;
    private int marginHedgedStrong;
    private int quotesDelay;
    private int swapOpenPrice;

    @Mapping("starting")
    private long startingTime;
    @Mapping("expiration")
    private long expirationTime;
    private long valueDate;

    private double filterLimit;
    private double swapLong;
    private double swapShort;
    private double contractSize;
    private double tickValue;
    private double tickSize;
    private double marginInitial;
    private double marginMaintenance;
    private double marginHedged;
    private double marginDivider;
    private double point;
    private double multiply;
    private double bidTickValue;
    private double askTickValue;

    private float filterReserved;

    @OneToMany(mappedBy = "conSymbol")
    private List<ConSessions> sessions;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String externalUnused; // it is sent as an int[7] by MT, so stored as json

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String unused; // it is sent as an int[21], so stored as json
}

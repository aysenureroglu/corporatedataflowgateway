package com.gkg.corporate_data_flow_gateway.mt4.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
public class ConGroupSec {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date transactionDate;

    private String metatraderId;
    @Mapping("show")
    private int showValue;
    private int trade;
    @Mapping("execution")
    private int executionNum;
    private int commType;
    private int commLots;
    private int commAgentType;
    private int spreadDiff;
    private int lotMin;
    private int lotMax;
    private int lotStep;
    private int ieDeviation;
    private int confirmation;
    private int tradeRights;
    private int ieQuickMode;
    private int autoCloseoutMode;
    private int commAgentLots;
    private int freeMarginMode;

    private double commBase;
    private double commTax;
    private double commAgent;

    @ManyToOne
    private ConGroup conGroup;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String reserved; // it is sent as an int[3] by MT, so stored as json
}

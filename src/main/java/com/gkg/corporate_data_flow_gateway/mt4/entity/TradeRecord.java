package com.gkg.corporate_data_flow_gateway.mt4.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class TradeRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;
    private String comment;

    private char symbol;
    private char convReserv;

    @Mapping("order")
    private int orderValue; //order ticket
    private int login; //The login of the client, to whom the order belongs
    private int digits;
    private int cmd;
    private int volume;
    private int state;
    private int openPrice;
    private int sl;
    private int tp;
    private int valueDate;
    private int magic; // The order ID set by the client's Expert Advisor
    private int internalId;
    private int activation;
    private int spread;
    private int next;

    private long openTime;
    private long closeTime;
    private long expiration;

    private double convRateOpenTime; // comes as double[0] conv_rates
    private double convRateCloseTime; // comes as double[1] conv_rates
    private double commission;
    private double commissionAgent;
    private double storage;
    private double closePrice;
    private double profit;
    private double taxes;
    private double marginRate;
}

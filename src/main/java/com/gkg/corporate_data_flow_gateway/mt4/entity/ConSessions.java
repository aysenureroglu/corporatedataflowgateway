package com.gkg.corporate_data_flow_gateway.mt4.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
public class ConSessions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date date;

    private String metatraderId;

    private int quoteOvernight;
    private int tradeOvernight;

    @OneToMany(mappedBy = "conSessions")
    private List<ConSession> Sessions; // can be trade or quote, check before querying

    @ManyToOne
    private ConSymbol conSymbol;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String reserved; // it is sent as an int[2] by MT, so stored as json
}

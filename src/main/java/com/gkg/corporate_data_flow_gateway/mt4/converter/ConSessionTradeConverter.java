package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.ConSession;
import com.gkg.corporate_data_flow_gateway.mt4.entity.Trade;
import com.gkg.mosDtoLibrary.mt4.ConSessionDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ConSessionTradeConverter {

    private DozerBeanMapper mapper;

    public ConSessionDto from(Trade conSession){
        return  mapper.map(conSession, ConSessionDto.class);
    }

    public Trade to(ConSessionDto conSessionDto){
        return  mapper.map(conSessionDto, Trade.class);
    }
}

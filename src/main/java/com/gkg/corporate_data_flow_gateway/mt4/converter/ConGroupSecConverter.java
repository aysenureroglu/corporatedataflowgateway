package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.ConGroupSec;
import com.gkg.mosDtoLibrary.mt4.ConGroupSecDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ConGroupSecConverter {

    private DozerBeanMapper mapper;

    public ConGroupSecDto from(ConGroupSec conGroupSec){
        return  mapper.map(conGroupSec, ConGroupSecDto.class);
    }

    public ConGroupSec to(ConGroupSecDto conGroupSecDto){
        return  mapper.map(conGroupSecDto, ConGroupSec.class);
    }
}

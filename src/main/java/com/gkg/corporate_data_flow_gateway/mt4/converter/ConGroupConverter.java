package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.ConGroup;
import com.gkg.mosDtoLibrary.mt4.ConGroupDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ConGroupConverter {

    private DozerBeanMapper mapper;

    public ConGroupDto from(ConGroup conGroup){
        return  mapper.map(conGroup, ConGroupDto.class);
    }

    public ConGroup to(ConGroupDto conGroupDto){
        return  mapper.map(conGroupDto, ConGroup.class);
    }
}

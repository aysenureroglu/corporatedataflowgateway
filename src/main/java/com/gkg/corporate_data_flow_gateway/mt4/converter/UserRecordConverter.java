package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.TradeRecord;
import com.gkg.corporate_data_flow_gateway.mt4.entity.UserRecord;
import com.gkg.mosDtoLibrary.mt4.TradeRecordDto;
import com.gkg.mosDtoLibrary.mt4.UserRecordDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class UserRecordConverter {

    private DozerBeanMapper mapper;

    public UserRecordDto from(UserRecord userRecord){
        return mapper.map(userRecord, UserRecordDto.class);
    }

    public UserRecord to(UserRecordDto userRecordDto){
        return mapper.map(userRecordDto, UserRecord.class);
    }
}

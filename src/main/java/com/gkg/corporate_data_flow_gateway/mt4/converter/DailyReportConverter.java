package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.DailyReport;
import com.gkg.mosDtoLibrary.mt4.DailyReportDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class DailyReportConverter {

    private DozerBeanMapper mapper;

    public DailyReportDto from(DailyReport dailyReport){
        return mapper.map(dailyReport, DailyReportDto.class);
    }

    public DailyReport to(DailyReportDto dailyReportDto){
        return mapper.map(dailyReportDto, DailyReport.class);
    }
}

package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.TradeRecord;
import com.gkg.mosDtoLibrary.mt4.TradeRecordDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class TradeRecordConverter {

    private DozerBeanMapper mapper;

    public TradeRecordDto from(TradeRecord tradeRecord){
        return mapper.map(tradeRecord, TradeRecordDto.class);
    }

    public TradeRecord to(TradeRecordDto tradeRecordDto){
        return mapper.map(tradeRecordDto, TradeRecord.class);
    }
}

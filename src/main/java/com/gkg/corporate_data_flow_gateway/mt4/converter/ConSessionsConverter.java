package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.ConSessions;
import com.gkg.mosDtoLibrary.mt4.ConSessionsDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ConSessionsConverter {

    private DozerBeanMapper mapper;


    public ConSessionsDto from(ConSessions conSessions){
        return  mapper.map(conSessions, ConSessionsDto.class);
    }

    public ConSessions to(ConSessionsDto conSessionsDto){
        return  mapper.map(conSessionsDto, ConSessions.class);
    }
}

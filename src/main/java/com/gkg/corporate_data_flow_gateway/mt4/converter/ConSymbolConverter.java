package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.ConSymbol;
import com.gkg.mosDtoLibrary.mt4.ConSymbolDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ConSymbolConverter {

    private DozerBeanMapper mapper;

    public ConSymbolDto from(ConSymbol conSymbol){
        return  mapper.map(conSymbol, ConSymbolDto.class);
    }

    public ConSymbol to(ConSymbolDto conSymbolDto){
        return  mapper.map(conSymbolDto, ConSymbol.class);
    }
}

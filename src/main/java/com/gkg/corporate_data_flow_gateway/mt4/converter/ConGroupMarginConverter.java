package com.gkg.corporate_data_flow_gateway.mt4.converter;

import com.gkg.corporate_data_flow_gateway.mt4.entity.ConGroupMargin;
import com.gkg.mosDtoLibrary.mt4.ConGroupMarginDto;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
public class ConGroupMarginConverter {

    private DozerBeanMapper mapper;

    public ConGroupMarginDto from(ConGroupMargin conGroupMargin){
        return  mapper.map(conGroupMargin, ConGroupMarginDto.class);
    }

    public ConGroupMargin to(ConGroupMarginDto conGroupMarginDto){
        return  mapper.map(conGroupMarginDto, ConGroupMargin.class);
    }
}

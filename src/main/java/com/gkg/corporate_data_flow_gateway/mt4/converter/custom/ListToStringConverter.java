package com.gkg.corporate_data_flow_gateway.mt4.converter.custom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.DozerConverter;

import java.util.List;

public class ListToStringConverter extends DozerConverter<List, String> {

    public ListToStringConverter(Class<List> prototypeA, Class<String> prototypeB) {
        super(prototypeA, prototypeB);
    }

    @Override
    public String convertTo(List integers, String s) {
        if(integers == null){
            return null;
        }
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(integers);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
        //return ((List<?>) integers).stream().map(e -> e.toString()).reduce("", String::concat);
    }

    @Override
    public List<Integer> convertFrom(String s, List integers) {
        if(s.isEmpty()){
            return null;
        }
        List<Integer> list = null;
        final ObjectMapper mapper = new ObjectMapper();
        try {
            list =mapper.readerForListOf(Integer.class).readValue(s);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return list;
    }
}